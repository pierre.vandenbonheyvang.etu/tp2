import Component from "./components/Component";

export default class Router {
  static titleElement;
  static contentElement;
  static routes;

  static navigate(path) {
    console.log("navigate");
    console.log(Router.routes[0].page);
    const title = new Component("h1", null, Router.routes[0].title);

    Router.titleElement.innerHTML = title.render();
    Router.contentElement.innerHTML = Router.routes[0].page.render();
  }
}
