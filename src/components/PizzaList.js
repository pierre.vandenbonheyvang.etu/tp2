import Component from "./Component.js";
import PizzaThumbnail from "./PizzaThumbnail.js";

export default class PizzaList extends Component {
  constructor(data) {
    const map = data.map((e) => new PizzaThumbnail(e));

    super("section", { name: "class", value: "pizzaList" }, map);
  }

  set pizzas(pizzas) {
    const mapPizzas = pizzas.map((e) => new PizzaThumbnail(e));
    this.children = mapPizzas;
  }
}
