import Component from "./Component.js";

export default class Img extends Component {
  constructor(value) {
    super("img", { name: "src", value: value });
  }
}
