export default class Component {
  tagName;
  attribute;
  children;
  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.attribute = attribute;
    this.children = children;
  }
  render() {
    let res = `<${this.tagName}`;

    //}else if (this.children != null && this.attribute?.name){}
    if (this.attribute && this.attribute.name) {
      res = res + ` ${this.attribute.name}="${this.attribute.value}"`;
    }
    if (this.children == null) {
    } else if (this.children instanceof Array) {
      res = res + this.renderChildren();
    } else if (!(this.children instanceof Array)) {
      res = res + `>${this.children}`;
    }
    res = res + `</${this.tagName}>`;
    return res;
  }

  renderChildren() {
    let chaine = "";
    this.children.forEach((e) => {
      if (e instanceof Component) {
        chaine = chaine + e.render();
      } else {
        chaine = chaine + e;
      }
    });
    return `>${chaine}`;
  }
}
